const express = require('express');
const app = express();
const scheduler = require('./schedule/scheduler');
require('./routes')(app, {});

const port = process.env.PORT || 34197;

scheduler.scheduleWeatherApi();

app.use(express.json());

app.use((request, response, next) => {
  console.log(request.headers)
  next()
});

app.get('/', (request, response) => {
  response.send('Welcome to Hackor SmartHome!')
});

app.use((err, request, response, next) => {
  console.log(err)
  response.status(500).send('Something went wrong!')
});

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`Server is listening on ${port}`)
});





