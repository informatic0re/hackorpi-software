const weatherData = require('../model/weatherData')
const dbConnection = require('../persistence/connection');

module.exports = {
    insert: insert,
    getLatest: getLatest
}

function insert(data) {
    console.log('weather data insert...');
    dbConnection.executeInsertUpdate("REPLACE INTO " + weatherData.TABLE_NAME +
        "("
        + weatherData.COL_ID + ","
        + weatherData.COL_TIMESTAMP + ","
        + weatherData.COL_PRESSURE + ","
        + weatherData.COL_HUMIDITY + ","
        + weatherData.COL_CLOUDS + ","
        + weatherData.COL_SUNRISE + ","
        + weatherData.COL_SUNSET + ","
        + weatherData.COL_TEMPERATURE + ","
        + weatherData.COL_TEMPERATURE_MIN + ","
        + weatherData.COL_TEMPERATURE_MAX
        + ") VALUES("
        + "1," //hardcoded primary key to ensure single row
        + "'" + data.timestamp + "',"
        + data.pressure + ","
        + data.humidity + ","
        + data.clouds + ","
        + data.sunrise + ","
        + data.sunset + ","
        + "'" + data.temp + "',"
        + "'" + data.temp_min + "',"
        + "'" + data.temp_max + "'"
        + ")");
}

function getLatest(resultCallback) {
    console.log('query latest weather data...');
    dbConnection.executeQuery(
        "SELECT "
        + weatherData.COL_ID + ","
        + weatherData.COL_TIMESTAMP + ","
        + weatherData.COL_PRESSURE + ","
        + weatherData.COL_HUMIDITY + ","
        + weatherData.COL_CLOUDS + ","
        + weatherData.COL_SUNRISE + ","
        + weatherData.COL_SUNSET + ","
        + weatherData.COL_TEMPERATURE + ","
        + weatherData.COL_TEMPERATURE_MIN + ","
        + weatherData.COL_TEMPERATURE_MAX
        + " FROM " + weatherData.TABLE_NAME
        + " ORDER BY " + weatherData.COL_ID + " DESC LIMIT 1",
        function (err, rows) {
            if (err) { console.error(err.message); }
            rows.forEach((row) => {
                console.log('latest weather data temperature = ' + row[weatherData.COL_TEMPERATURE]);
                resultCallback(row);
            })
        });
}