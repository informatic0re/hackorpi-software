const weatherData = require('../model/weatherData')
const sqlite3 = require('sqlite3')
const fs = require('fs')

const DB_PATH = './db/hackorpi.db'

const sqLiteConnection = openCreateDb();

module.exports = {
    close: close,
    executeQuery: executeQuery,
    executeInsertUpdate: executeInsertUpdate
}

function close() {
    sqLiteConnection.close((err) => {
        if (err) {
            console.error(err.message);
        }
        console.log('Closed the database connection.');
    });
}

function executeInsertUpdate(sql) {
    sqLiteConnection.run(sql);
}

function executeQuery(sql, callback) {
    return sqLiteConnection.all(sql, callback);
}

function openCreateDb() {
    if (fs.existsSync(DB_PATH)) {
        return doOpen(sqlite3.OPEN_READWRITE);
    }

    let connection = doOpen(sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE);
    runDdl(connection);
    return connection;
}

function runDdl(connection) {
    createWeatherTable(connection);
}

function createWeatherTable(connection) {
    const sqlCreate = `CREATE TABLE ` + weatherData.TABLE_NAME + ` (
            `+ weatherData.COL_ID + ` INTEGER PRIMARY KEY,
            `+ weatherData.COL_TIMESTAMP + ` TEXT,
            `+ weatherData.COL_TEMPERATURE + ` TEXT,
            `+ weatherData.COL_TEMPERATURE_MIN + ` TEXT,
            `+ weatherData.COL_TEMPERATURE_MAX + ` TEXT,
            `+ weatherData.COL_PRESSURE + ` INTEGER,
            `+ weatherData.COL_HUMIDITY + ` INTEGER,
            `+ weatherData.COL_CLOUDS + ` INTEGER,
            `+ weatherData.COL_SUNRISE + ` INTEGER,
            `+ weatherData.COL_SUNSET + ` INTEGER
        )`;
    connection.run(sqlCreate);
    console.log('Table ' + weatherData.TABLE_NAME + ' created');
}

function doOpen(flags) {
    return new sqlite3.Database(DB_PATH, flags, (err) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log('Connected to database: ' + DB_PATH);
    });
}
