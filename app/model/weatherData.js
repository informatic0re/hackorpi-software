module.exports = Object.freeze({
    TABLE_NAME: 'weather_data',
    COL_ID: 'id',
    COL_TIMESTAMP: 'timestamp',
    COL_TEMPERATURE: 'temperature',
    COL_TEMPERATURE_MIN: 'temperature_min',
    COL_TEMPERATURE_MAX: 'temperature_max',
    COL_PRESSURE: 'pressure',
    COL_HUMIDITY: 'humidity',
    COL_CLOUDS: 'clouds',
    COL_SUNRISE: 'sunrise',
    COL_SUNSET: 'sunset',
    from: from
});

const ONE_HOUR = 3600000;

class WeatherData {
    constructor(timestamp, temp, temp_min, temp_max, pressure, humidity, clouds, sunrise, sunset) {
        this.timestamp = timestamp;
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.pressure = pressure;
        this.humidity = humidity;
        this.clouds = clouds;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }
}

function from(json) {
    return new WeatherData(
        new Date(Date.now() + ONE_HOUR).toUTCString(), //TODO find a better way, thats shit
        json.main.temp,
        json.main.temp_min,
        json.main.temp_max,
        json.main.pressure,
        json.main.humidity,
        json.clouds.all,
        json.sys.sunrise,
        json.sys.sunset
    );
}