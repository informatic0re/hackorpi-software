const superagent = require('superagent');
const weatherData = require('../model/weatherData');
const weatherDataDao = require('../persistence/weatherDataDao');
const nodeSchedule = require('node-schedule');

//const SCHEDULE_INTERVAL = '*/60 * * * * *';
const SCHEDULE_INTERVAL = '0 0 */1 * *';

module.exports = {
    scheduleWeatherApi: scheduleWeatherApi
}

function scheduleWeatherApi() {
    let callback = (err, res) => {
        if (err) { return console.log(err); }

        //console.log(res.body);
        console.log('Received weather data from API...');
        let data = weatherData.from(res.body);
        weatherDataDao.insert(data);
    };

    schedule(function () {
        executeWeatherApiCall(callback);
    });

    //execute always once when scheduled
    executeWeatherApiCall(callback);
}

function executeWeatherApiCall(callback) {
    //api.openweathermap.org/data/2.5/weather?q=Berlin,de&appid=86e89bd2d3489bb5b276256ba21d18de
    superagent.get('https://api.openweathermap.org/data/2.5/weather')
        .query({ appid: '86e89bd2d3489bb5b276256ba21d18de', q: 'Berlin,de', units: 'metric', lang: 'de' })
        .end(callback);
}

function schedule(task) {
    nodeSchedule.scheduleJob(SCHEDULE_INTERVAL, task);
}