const weatherDataDao = require('../persistence/weatherDataDao');
const weatherData = require('../model/weatherData');

module.exports = function (app, db) {

    app.get('/api/metrics', (request, response) => { //get display data
        weatherDataDao.getLatest(function (result) {
            console.log(result);
            response.send(JSON.stringify(result));
        });

    })

};