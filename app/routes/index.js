const temperatureRoutes = require('./temperatureRoutes');
const metricRoutes = require('./metricRoutes');

module.exports = function(app, db) {
    temperatureRoutes(app, db);
    metricRoutes(app, db);
  // Other route groups could go here, in the future
};